<?php 
//  session_start();
	include "../php/dbconnect.php";
  if ((!isset($_SESSION['member_login']) & !isset($_SESSION['admin_login']))) {
    # code...
//    die('Please <a href="../index.php">login</a> to view this page');
  	header('Location: ../index.php');
  }
 ?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Anitorium</title>

	<link rel="stylesheet" href="../css/bootstrap.css">
	<script src="../js/jquery.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/w3data.js"></script>
	<style type="text/css">
		  .carousel-inner > .item > img,
		  .carousel-inner > .item > a > img {
		      width: 70%;
		      margin: auto;
		  }
		body {
			background-color: #141414;
			width: 80%;
			margin: auto;
			color: white;
		}
		hr {
			border-color: black;
		}
	</style>
</head>
<body>
	<div class="container center-block">
	<div w3-include-html="header.php"></div>
	<script>
		w3IncludeHTML();
	</script>

	<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
    <li data-target="#myCarousel" data-slide-to="3"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="../media/image/img.jpg" alt="Chania">
    </div>

    <div class="item">
      <img src="../media/image/img.jpg" alt="Chania">
    </div>

    <div class="item">
      <img src="../media/image/img.jpg" alt="Flower">
    </div>

    <div class="item">
      <img src="../media/image/img.jpg" alt="Flower">
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
	<div class="row" style="color:#9e9e9e;margin-top:2%;">
		<div class="col-md-4" style="border-left:5px solid #990000;">
			<h3>Latest in Forum</h3>
			<hr>
				<div class="col-md-8">
					<a href="#"><h6>Forum Title</h6></a>
					<p>User</p>
					<p>Comment</p>
				</div>
		</div>
		<div class="col-md-4" style="border-left:5px solid #990000;">
			<h3>Popular Anime</h3>
			<hr>
			<div class="row">
				<div class="col-md-4">
					<img src="../media/image/img.jpg" width="100px"/>
				</div>
				<div class="col-md-8">
					<a href="#"><h6>Anime Name</h6></a>
					<p>Rate</p>
					<p>Genre</p>
				</div>
			</div>
						<div class="row">
				<div class="col-md-4">
					<img src="../media/image/img.jpg" width="100px"/>
				</div>
				<div class="col-md-8">
					<a href="#"><h6>Anime Name</h6></a>
					<p>Rate</p>
					<p>Genre</p>
				</div>
			</div>
						<div class="row">
				<div class="col-md-4">
					<img src="../media/image/img.jpg" width="100px"/>
				</div>
				<div class="col-md-8">
					<a href="#"><h6>Anime Name</h6></a>
					<p>Rate</p>
					<p>Genre</p>
				</div>
			</div>
						<div class="row">
				<div class="col-md-4">
					<img src="../media/image/img.jpg" width="100px"/>
				</div>
				<div class="col-md-8">
					<a href="#"><h6>Anime Name</h6></a>
					<p>Rate</p>
					<p>Genre</p>
				</div>
			</div>
		</div>
		<div class="col-md-4" style="border-left:5px solid #990000;">
			<h3>Top Anime</h3>
			<hr>
			<div class="row">
				<div class="col-md-4">
					<img src="../media/image/img.jpg" width="100px"/>
				</div>
				<div class="col-md-8">
					<a href="#"><h6>Anime Name</h6></a>
					<p>Rate</p>
					<p>Genre</p>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>