<?php session_start(); ?>
<div class="row">
		<div class="col-md-12">
			<nav class="navbar navbar-default navbar-fixed-top navbar-inverse" role="navigation">
				<div class="navbar-header">
					 
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						 <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
					</button> <a class="navbar-brand" href="../index.html">Anitorium</a>
				</div>
				
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li>
							<a href="user.php">My Profile</a>
						</li>
						<li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown">Anime<strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="#">All Anime</a>
								</li>
								<li>
									<a href="#">Top Anime</a>
								</li>
								<li>
									<a href="#">Most Popular</a>
								</li>
								<li class="divider">
								</li>
							</ul>
						</li>
						<li>
							<a href="#">Messages</a>
						</li>
						<li>
							<a href="#">Forum</a>
						</li>
						<li>
							<a href="#">Chatorium</a>
						</li>
						<?php 
							if (isset($_SESSION['admin_login'])) {
								echo '<li>
										<a href="../admin/admin.php">Admin</a>
									</li>';
							}
						?>
						<span><?php echo $_SESSION['name'] ?></span>
					</ul>
					<form class="navbar-form navbar-right" role="search">
						<div class="form-group">
							<input type="text" class="form-control" />
						</div> 
						<button type="submit" class="btn btn-default">
							Submit
						</button>
					</form>
				</div>
				
			</nav>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<br>
			<br>
			<br>
		</div>
	</div>