<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Anime</title>

	<link rel="stylesheet" href="../css/bootstrap.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script src="http://www.w3schools.com/lib/w3data.js"></script>
	<style type="text/css">
		  .carousel-inner > .item > img,
		  .carousel-inner > .item > a > img {
		      width: 70%;
		      margin: auto;
		  }
		body {
			background-color: #141414;
			width: 80%;
			margin: auto;
			color: #9e9e9e;
		}
		hr {
			border-color: black;
		}
	</style>
</head>
<body>
	<div class="container center-block">
	<div w3-include-html="header.php"></div>
	<script>
		w3IncludeHTML();
	</script>
	
	</div>
</body>
</html>