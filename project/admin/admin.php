<!DOCTYPE html>
<?php 
	include "../php/dbconnect.php";
  if (!isset($_SESSION['admin_login'])) {
    # code...
    die('Please <a href="..\index.php">Login</a> as admin to view this page');
  }
 ?>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Admin Panel</title>

	<link rel="stylesheet" href="../css/bootstrap.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script src="http://www.w3schools.com/lib/w3data.js"></script>
	<style type="text/css">
		  .carousel-inner > .item > img,
		  .carousel-inner > .item > a > img {
		      width: 70%;
		      margin: auto;
		  }
		body {
			background-color: #141414;
			width: 80%;
			margin: auto;
			color: white;
		}
		hr {
			border-color: black;
		}
	</style>
</head>
<body>
	<div class="container center-block">
	<div class="row">
		<div class="col-md-12">
			<nav class="navbar navbar-default navbar-fixed-top navbar-inverse" role="navigation">
				<div class="navbar-header">
					 
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						 <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
					</button> <a class="navbar-brand" href="../index.html">Anitorium</a>
				</div>
				<div class="row">
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li>
							<a href="user.php">My Profile</a>
						</li>
						<li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown">Anime<strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="#">All Anime</a>
								</li>
								<li>
									<a href="#">Top Anime</a>
								</li>
								<li>
									<a href="#">Most Popular</a>
								</li>
								<li class="divider">
								</li>
							</ul>
						</li>
						<li>
							<a href="#">Messages</a>
						</li>
						<li>
							<a href="#">Forum</a>
						</li>
						<li>
							<a href="#">Chatorium</a>
						</li>
						<li>
							<a href="#">Admin</a>
						</li>
						<span><?php echo $_SESSION['name'] ?></span>
					</ul>
					<form class="navbar-form navbar-right" role="search">
						<div class="form-group">
							<input type="text" class="form-control" />
						</div> 
						<button type="submit" class="btn btn-default">
							Submit
						</button>
					</form>
				</div>
				</div>
				<div class="row" style="margin-left:6.5%;">
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li>
							<a href="#" onclick="show('user_approval')" >User Approval</a>
						</li>
						<li>
							<a href="#" onclick="show('comment_approval')">Comment Approval</a>
						</li>
						<li>
							<a href="#" onclick="show('thread_approval')">Thread Post Approval</a>
						</li>
						<li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown">Anime Stuffs<strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="#" onclick="show('upload_anime')">Upload Anime</a>
								</li>
								<li>
									<a href="#" onclick="show('anime_list')">Anime List</a>
								</li>
							</ul>
						</li>
						<li>
							<a href="#" onclick="show('admin_create')">Create Admin</a>
						</li>
						<li>
							<a href="#" onclick="show('users_list')">Users List</a>
						</li>
					</ul>
				</div>
				</div>
			</nav>
		</div>
	</div>
	<script type="text/javascript">
		function show(show) {
			document.getElementById('user_approval').style.display='none';
			document.getElementById('comment_approval').style.display='none';
			document.getElementById('thread_approval').style.display='none';
			document.getElementById('upload_anime').style.display='none';
			document.getElementById('admin_create').style.display='none';
			document.getElementById('users_list').style.display='none';
			document.getElementById('car_control').style.display='none';
			document.getElementById('anime_list').style.display='none';

			document.getElementById(show).style.display='block';
		}
	</script>
	<div class="row">
		<div class="col-md-12">
			<br>
			<br>
			<br>
			<br>
			<br>
		</div>
	</div>
	<div class="row" id="user_approval" style="display:none;">
	<h4>User Approval</h4>
		<div class="col-md-12" style="border-left:5px solid #990000;">
		<?php 
		$sql = "SELECT max(id) FROM user";
		$row = mysqli_query($conn,$sql);
		$count = mysqli_fetch_assoc($row);
		$maxid = (int)$count['max(id)'];
		$sql = "SELECT * FROM user WHERE userType='new'";
		$result = mysqli_query($conn,$sql);
		echo '<form class="form-horizontal" role="form" method="post" action="'.htmlspecialchars($_SERVER["PHP_SELF"]).'">
				<table class="table-bordered" style="text-align:center;">
				<thead>
					<th>Name</th>
					<th>Username</th>
					<th>Date of Birth</th>
					<th>E-mail</th>
					<th>Approve</th>
				</thead>
				<tbody>';
		while($row = mysqli_fetch_array($result)) {

			echo sprintf('<tr>
					<input type="hidden" name="id" value="%d">
					<td>%s</td>
					<td>%s</td>
					<td>%s</td>
					<td>%s</td>
					<td><input type="checkbox" name="user[%d]" value="%d"></td>
				</tr>',$row["id"], $row["name"], $row["username"], $row["dob"], $row["email"], $row["id"], $row["id"]);
		}
		echo '</tbody></table>
				<input type="submit" class="btn btn-default" name="user_approval" value="Approve">
				<input type="submit" class="btn btn-default" name="user_approval_all" value="Approve All">
				</form>';

		if(isset($_POST['user_approval_all'])) {
			$sql = "UPDATE user SET userType='member'";
			mysqli_query($conn, $sql);
		}
		if(isset($_POST['user_approval'])) {
			$var = $_POST['user'];
			for($i = 0; $i <= $maxid; $i++) {
				if(!empty($var[$i])) {
					$sql = "UPDATE user SET userType='member' WHERE id=".$var[$i];
					mysqli_query($conn, $sql);
				}
			}
		}
		?>

		</div>
	</div>
	<div class="row" id="comment_approval" style="display:none;">
	<h4>Comment Approval</h4>
		<div class="col-md-12" style="border-left:5px solid #990000;">
		<?php 
		$sql = "SELECT max(id) FROM anime_comment";
		$row = mysqli_query($conn,$sql);
		$count = mysqli_fetch_assoc($row);
		$maxid = (int)$count['max(id)'];
		$sql = "SELECT anime_comment.id AS id, user.name AS user_name, anime.name AS anime_name, anime_comment.comment AS comment, anime_comment.post_time AS post_time FROM anime_comment, anime, user WHERE anime.id=anime_comment.anime_id AND user.id = anime_comment.user_id AND approval_status='new'";
		$result = mysqli_query($conn,$sql);
		echo '<form class="form-horizontal" role="form" method="post" action="'.htmlspecialchars($_SERVER["PHP_SELF"]).'">
				<table class="table-bordered" style="text-align:center;">
				<thead>
					<th>Name</th>
					<th>Comment</th>
					<th>Date</th>
					<th>Anime</th>
					<th>Approve</th>
				</thead>
				<tbody>';
		while($row = mysqli_fetch_array($result)) {

			echo sprintf('<tr>
					<input type="hidden" name="id" value="%d">
					<td>%s</td>
					<td>%s</td>
					<td>%s</td>
					<td>%s</td>
					<td><input type="checkbox" name="user[%d]" value="%d"></td>
				</tr>',$row["id"], $row["user_name"], $row["comment"], $row["post_time"], $row["anime_name"],$row["id"],$row["id"]);
		}
		echo '</tbody></table>
				<input type="submit" class="btn btn-default" name="comment_approval" value="Approve">
				<input type="submit" class="btn btn-default" name="comment_approval_all" value="Approve All">
				</form>';
		if(isset($_POST['comment_approval_all'])) {
			$sql = "UPDATE anime_comment SET approval_status='approved'";
			mysqli_query($conn, $sql);
		}

		if(isset($_POST['comment_approval'])) {
			$var = $_POST['user'];
			for($i = 0; $i <= $maxid; $i++) {
				if(!empty($var[$i])) {
					$sql = "UPDATE anime_comment SET approval_status='approved' WHERE id=".$var[$i];
					mysqli_query($conn, $sql);
				}
			}
		}
		?>		
		</div>
	</div>
	<div class="row" id="thread_approval" style="display:none;">
	<h4>Thread Post Approval</h4>
		<div class="col-md-12" style="border-left:5px solid #990000;">
		<?php 
		$sql = "SELECT max(id) FROM forum";
		$row = mysqli_query($conn,$sql);
		$count = mysqli_fetch_assoc($row);
		$maxid = (int)$count['max(id)'];
		$sql = "SELECT forum.id AS id, forum.title AS title, forum.post_time AS post_time, user.name AS name FROM forum, user WHERE forum.user_id = user.id AND approval_status='new'";
		$result = mysqli_query($conn,$sql);
		echo '<form class="form-horizontal" role="form" method="post" action="'.htmlspecialchars($_SERVER["PHP_SELF"]).'">
				<table class="table-bordered" style="text-align:center;">
				<thead>
					<th>Title</th>
					<th>Created By</th>
					<th>Post Date</th>
					<th>Approve</th>
				</thead>
				<tbody>';
		while($row = mysqli_fetch_array($result)) {

			echo sprintf('<tr>
					<input type="hidden" name="id" value="%d">
					<td>%s</td>
					<td>%s</td>
					<td>%s</td>
					<td><input type="checkbox" name="user[%d]" value="%d"></td>
				</tr>',$row["id"], $row["title"],$row["name"], $row["post_time"],$row["id"],$row["id"]);
		}
		echo '</tbody></table>
				<input type="submit" class="btn btn-default" name="thread_approval" value="Approve">
				<input type="submit" class="btn btn-default" name="thread_approval_all" value="Approve All">
				</form>';
		if(isset($_POST['thread_approval_all'])) {
			$sql = "UPDATE forum SET approval_status='approved'";
			mysqli_query($conn, $sql);
		}
		if(isset($_POST['thread_approval'])) {
			$var = $_POST['user'];
			for($i = 0; $i <= $maxid; $i++) {
				if(!empty($var[$i])) {
					$sql = "UPDATE forum SET approval_status='approved' WHERE id=".$var[$i];
					mysqli_query($conn, $sql);
				}
			}
		}
		?>			
		</div>
		<div class="col-md-12" style="border-left:5px solid #990000;">
		<?php 
		$sql = "SELECT max(id) FROM forum_comment";
		$row = mysqli_query($conn,$sql);
		$count = mysqli_fetch_assoc($row);
		$maxid = (int)$count['max(id)'];
		$sql = "SELECT user.name AS user_name, forum_comment.id AS id, forum_comment.comment AS comment, forum_comment.thread_id as thread_id, forum_comment.post_time AS post_time FROM forum_comment, user WHERE forum_comment.user_id=user.id AND approval_status='new'";
		$result = mysqli_query($conn,$sql);
		echo '<form class="form-horizontal" role="form" method="post" action="'.htmlspecialchars($_SERVER["PHP_SELF"]).'">
				<table class="table-bordered" style="text-align:center;">
				<thead>
					<th>Comment</th>
					<th>Commented By</th>
					<th>Thread ID</th>
					<th>Date</th>
					<th>Approve</th>
				</thead>
				<tbody>';
		while($row = mysqli_fetch_array($result)) {

			echo sprintf('<tr>
					<input type="hidden" name="id" value="%d">
					<td>%s</td>
					<td>%s</td>
					<td>%s</td>
					<td>%s</td>
					<td><input type="checkbox" name="user[%d]" value="%d"></td>
				</tr>',$row["id"], $row["comment"], $row["user_name"], $row["thread_id"], $row["post_time"],$row["id"],$row["id"]);
		}
		echo '</tbody></table>
				<input type="submit" class="btn btn-default" name="forum_comment_approval" value="Approve">
				<input type="submit" class="btn btn-default" name="forum_comment_approval_all" value="Approve All">
				</form>';
		if(isset($_POST['forum_comment_approval'])) {
			$sql = "UPDATE forum_comment SET approval_status='approved'";
			mysqli_query($conn, $sql);
		}

		if(isset($_POST['forum_comment_approval'])) {
			$var = $_POST['user'];
			for($i = 0; $i <= $maxid; $i++) {
				if(!empty($var[$i])) {
					$sql = "UPDATE forum_comment SET approval_status='approved' WHERE id=".$var[$i];
					mysqli_query($conn, $sql);
				}
			}
		}
		?>			
		</div>
	</div>
	<div class="row" id="car_control" style="display:none;">
	<h4>Carousel Control</h4>
		<div class="col-md-12" style="border-left:5px solid #990000;">
		</div>
	</div>
	<div class="row" id="upload_anime" style="display:none;">
	<h4>Upload Anime</h4>
		<div class="col-md-12" style="border-left:5px solid #990000;">
			<form class="form-horizontal" role="form" style="margin-top: 1%" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data">
				<div class="form-group">
			      <div class="col-sm-4" style="margin-right: 0px; padding-right: 5px;">
			        <input type="text" class="form-control" placeholder="Anime Name" name="name" value="sth" required>
			      </div>
			    </div>
			    <div class="form-group">
			      <div class="col-sm-8">
			      	<textarea placeholder="Description" name="description" class="form-control form-group" required>sdfsdf</textarea>
			      </div>
			    </div>
			    <div class="form-group">
			      <div class="col-sm-8">
			        <input type="date" class="form-control" placeholder="Date of Birth: " name="dor" value="2011/01/01" required>
			      </div>
			    </div>
			    <div class="form-group">
			      <div class="col-sm-8">
			        <select name="status" class="form-control">
			        	<option class="form-control" disabled>Status</option>
			        	<option class="form-control" value="airing">Airing</option>
			        	<option class="form-control" value="aired">Aired</option>
			        	<option class="form-control" value="not_aired">Not Aired</option>
			        </select>
			      </div>
			    </div>
			    <div class="form-group">
			      <div class="col-sm-8">
			      	<input class="form-group form-control" type="file" id="file" name="files[]" multiple="multiple" />
			      </div>
			    </div>
			    <div class="form-group">        
			      <div class="col-sm-10">
			        <input type="submit" name="upload" value="Upload" class="btn btn-default">
			      </div>
			    </div>
			  </form>
			  <?php 
			  	if(isset($_POST["upload"])) {
			  		$sql = sprintf("INSERT INTO anime (name, summary, date_of_release, status) VALUES ('%s','%s','%s','%s')",$_POST['name'],$_POST['description'],$_POST['dor'],$_POST['status']);
							if ($conn->query($sql) === TRUE) {
								$last_id = $conn->insert_id;
							    echo "Anime Upload Successfull";
							} else {
							    echo "Error creating TABLE: " . $conn->error;
							}

					mkdir("../media/anime/".$last_id);
					$target_dir="../media/anime/".$last_id."/";
					$valid_formats = array("mp4", "wav", "jpg", "mkv", "avi");
					$count = 0;
					
					foreach ($_FILES['files']['name'] as $f => $name) {
						$ext = pathinfo($name, PATHINFO_EXTENSION);

					    if ($_FILES['files']['error'][$f] == 4) {
					        continue; // Skip file if any error found
					    }	       
					    if ($_FILES['files']['error'][$f] == 0) {	           
							if( ! in_array($ext, $valid_formats) ){
								$message[] = "$name is not a valid format";
								continue; // Skip invalid file formats
							}
					        else{ // No error found! Move uploaded files 
					        	//echo $path.$name;
					        	//echo "this : ".$target_dir.$_FILES["files"]["name"][$f]."  this// ";
					            if(move_uploaded_file($_FILES["files"]["tmp_name"][$f], $target_dir.$_FILES["files"]["name"][$f]))
					            $count++; // Number of successfully uploaded file
					        }
					    }
					}
					//echo $count." files has been uploaded in the slider";
					$sql = "UPDATE anime SET number_of_eps=".$count." WHERE id=".$last_id;
					mysqli_query($conn, $sql);
				
				}
			  ?>
		</div>
	</div>
	<div class="row" id="anime_list" style="display:none;">
	<h4>Anime List</h4>
		<div class="col-md-12" style="border-left:5px solid #990000;">
			<?php 
			$sql = "SELECT * FROM anime";
		$result = mysqli_query($conn,$sql);
		echo '<table class="table-bordered" style="text-align:center;">
				<thead>
					<th>ID.</th>
					<th>Name</th>
					<th>Rate</th>
					<th>Summary</th>
					<th>Date of Release</th>
					<th>Total Episodes</th>
					<th>Status</th>
					<th>Modify</th>
					<th>Delete</th>
				</thead>
				<tbody>';
		while($row = mysqli_fetch_array($result)) {

			echo sprintf('<form class="form-horizontal" role="form" method="post" action="'.htmlspecialchars($_SERVER["PHP_SELF"]).'" enctype="multipart/form-data">
				<tr><td><input type="hidden" value="%d" name="id">%d</td>
					<td><input type="text" class="form-control" name="name" value="%s"></td>
					<td>%.2f</td>
					<td><textarea name="summary" class="form-group form-control">%s</textarea></td>
					<td><input class="form-control" type="date" name="dor" value="%s"></td>
					<td><input type="hidden" value="%d" name="noe">%d</td>
					<td>
					<select name="status" class="form-control">
			        	<option class="form-control" value="%s">%s</option>
			        	<option class="form-control" value="airing">Airing</option>
			        	<option class="form-control" value="aired">Aired</option>
			        	<option class="form-control" value="not_aired">Not Aired</option>
			        </select>
					</td>
					<td><input class="form-group form-control" type="file" id="file" name="files[]" multiple="multiple" /></td>
					<td><input type="submit" class="btn btn-default"  name="delete_anime" value="Delete"/></td>
					<td><input type="submit" class="btn btn-default" name="update_anime" value="Update"/></td>
				</tr></form>', $row["id"], $row["id"], $row["name"], $row["rate"], $row["summary"], $row["date_of_release"], $row["number_of_eps"],$row["number_of_eps"], $row["status"], $row["status"]);
				}
				echo '</tbody></table>';

				if(isset($_POST["delete_anime"])) {
					echo "delete";
					$sql = "DELETE FROM anime WHERE id=".$_POST["id"];
					mysqli_query($conn, $sql);
				}

				if(isset($_POST["update_anime"])) {
					$sql = sprintf("UPDATE anime SET name='%s', summary='%s', date_of_release='%s', status='%s' WHERE id=%d", $_POST["name"], $_POST["summary"], $_POST["dor"], $_POST["status"], $_POST["id"]);
					if ($conn->query($sql) === TRUE) {
						$last_id = $conn->insert_id;
					    echo "Anime Upload Successfull";
					} else {
					    echo "Error creating TABLE: " . $conn->error;
					}


					$target_dir="../media/anime/".$_POST["id"]."/";
					$valid_formats = array("mp4", "wav", "jpg", "mkv", "avi");
					$count = 0;
					
					foreach ($_FILES['files']['name'] as $f => $name) {
						$ext = pathinfo($name, PATHINFO_EXTENSION);

					    if ($_FILES['files']['error'][$f] == 4) {
					        continue; // Skip file if any error found
					    }	       
					    if ($_FILES['files']['error'][$f] == 0) {	           
							if( ! in_array($ext, $valid_formats) ){
								$message[] = "$name is not a valid format";
								continue; // Skip invalid file formats
							}
					        else{ // No error found! Move uploaded files 
					        	//echo $path.$name;
					        	//echo "this : ".$target_dir.$_FILES["files"]["name"][$f]."  this// ";
					            if(move_uploaded_file($_FILES["files"]["tmp_name"][$f], $target_dir.$_FILES["files"]["name"][$f]))
					            $count++; // Number of successfully uploaded file
					        }
					    }
					}

					$total = $_POST["noe"] + $count;
					$sql = "UPDATE anime SET number_of_eps=".$total." WHERE id=".$_POST["id"];
					mysqli_query($conn, $sql);
				}
			 ?>
		</div>
	</div>
	<div class="row" id="admin_create" style="display:none;">
	<h4>Create Admin</h4>
		<div class="col-md-12" style="border-left:5px solid #990000;">
			<form class="form-horizontal" role="form" style="margin-top: 1%" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
				<div class="form-group">
			      <div class="col-sm-4" style="margin-right: 0px; padding-right: 5px;">
			        <input type="text" class="form-control" placeholder="First Name" name="firstname" required>
			      </div>

			      <div class="col-sm-4" style="margin-left: 0px;padding-left: 5px;">
			        <input type="text" class="form-control" placeholder="Last Name" name="lastname" required>
			      </div>
			    </div>
			    <div class="form-group">
			      <div class="col-sm-8">
			        <input type="date" class="form-control" placeholder="Date of Birth: " name="dob" required>
			      </div>
			    </div>
			    <div class="form-group">
			      <div class="col-sm-8">
			        <input type="email" class="form-control" id="email" placeholder="E-mail" name="email" required>
			      </div>
			    </div>
			    <div class="form-group">
			      <div class="col-sm-8">
			        <input type="text" class="form-control" placeholder="Username" name="username" required>
			      </div>
			    </div>
			    <div class="form-group">
			      <div class="col-sm-8">          
			        <input type="password" class="form-control" placeholder="Enter password" name="password" required>
			      </div>
			    </div>
			    <div class="form-group">
			      <div class="col-sm-8">          
			        <input type="password" class="form-control" placeholder="Confirm password" name="confirmpassword" required>
			      </div>
			    </div>
			    <div class="form-group">        
			      <div class="col-sm-10">
			        <input type="submit" name="signup" value="Sign Up" class="btn btn-default">
			      </div>
			    </div>
			  </form>
		</div>
		<?php 
		if(isset($_POST['signup'])) {
				$name = $_POST['firstname']." ".$_POST['lastname'];
				$username = $_POST['username'];
				$dob = $_POST['dob'];
				$password = md5($_POST['password']);
				$confirmpassword = md5($_POST['confirmpassword']);
				$email = $_POST['email'];

				if($password != $confirmpassword) {
					echo "try again";
				}
				else {
					$sql = sprintf("SELECT * FROM user WHERE name='%s'",$username);
					$check_user = mysqli_query($conn, $sql);
					if(mysqli_num_rows($check_user) == 1) {
						echo "username already exists";
					}
					else {
						$sql = sprintf("SELECT * FROM user WHERE email='%s'",$email);
						$check_user = mysqli_query($conn, $sql);
						if(mysqli_num_rows($check_user) == 1) {
							echo "email already exists";
						}
						else {
							$sql = sprintf("INSERT INTO user (name, username, dob, email, userType, password) VALUES ('%s','%s','%s','%s','admin','%s');", $name,$username,$dob,$email, $password);
							if ($conn->query($sql) === TRUE) {
							    echo "Registration Successfull";
							} else {
							    echo "Error creating TABLE: " . $conn->error;
							}
						}
					}
				}
			}
		?>
	</div>
	<div class="row" id="users_list" style="display:none;">
	<h4>Users List</h4>
		<div class="col-md-12" style="border-left:5px solid #990000;">
		<?php 
		$sql = "SELECT max(id) FROM user";
		$row = mysqli_query($conn,$sql);
		$count = mysqli_fetch_assoc($row);
		$maxid = (int)$count['max(id)'];
		$sql = "SELECT * FROM user ORDER BY userType";
		$result = mysqli_query($conn,$sql);
		echo '<table class="table-bordered" style="text-align:center;">
				<thead>
					<th>Name</th>
					<th>Username</th>
					<th>Date of Birth</th>
					<th>E-mail</th>
					<th>User Type</th>
					<th>Delete</th>
					<th>Adminship</th>
				</thead>
				<tbody>';
		while($row = mysqli_fetch_array($result)) {

			echo sprintf('<form class="form-horizontal" role="form" method="post" action="'.htmlspecialchars($_SERVER["PHP_SELF"]).'">
				<tr><input type="hidden" value="%d" name="id">
					<td>%s</td>
					<td>%s</td>
					<td>%s</td>
					<td>%s</td>
					<td>%s</td>
					<td><input type="submit" class="btn btn-default"  name="delete" value="Delete"/></td>
					<td><input type="submit" class="btn btn-default" name="admin" value="Make"/></td>
				</tr></form>', $row["id"], $row["name"], $row["username"], $row["dob"], $row["email"], $row["userType"]);
		}
		echo '</tbody></table>';

		if(isset($_POST["delete"])) {
			echo "delete";
			$sql = "DELETE FROM user WHERE id=".$_POST["id"];
			mysqli_query($conn, $sql);
		}


		if(isset($_POST["admin"])) {
			echo "delete";
			$sql = "UPDATE user SET userType='admin' WHERE id=".$_POST["id"];
			mysqli_query($conn, $sql);
		}

		if(isset($_POST['user_approval'])) {
			$var = $_POST['user'];
			for($i = 0; $i <= $maxid; $i++) {
				if(!empty($var[$i])) {
					$sql = "UPDATE user SET userType='member' WHERE id=".$var[$i];
					mysqli_query($conn, $sql);
				}
			}
		}

		
		?>
		</div>
	</div>
	</div>
</body>
</html>