<?php include "php/dbconnect.php" ?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Anitorium</title>

	<link rel="stylesheet" href="css/bootstrap.css">
	<style type="text/css">
		body {
			background-color: #141414;
		}
	</style>
</head>
<body>
	<div class="container center-block" style="margin-top: 12%;">
		<div class="col-md-6 login">
			<form class="form-horizontal" role="form" style="margin-top: 10%;" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
			    <div class="form-group">
			      <div class="col-sm-8 pull-right">
			        <input type="text" class="form-control" placeholder="Username" name="username" required>
			      </div>
			    </div>
			    <div class="form-group">
			      <div class="col-sm-8  pull-right">          
			        <input type="password" class="form-control" placeholder="Enter password" name="password" required>
			      </div>
			    </div>
			    <!-- <div class="form-group">        
			      <div class="col-sm-offset-2 col-sm-10">
			        <div class="checkbox pull-right">
			          <label><input type="checkbox"> Remember me</label>
			        </div>
			      </div>
			    </div> -->
			    <div class="form-group">        
			      <div class="col-sm-offset-2 col-sm-10">
			        <input type="submit" name="login" value="Login" class="btn btn-default pull-right">
			      </div>
			    </div>
			  </form>
		</div>
		<div class="col-md-6 signup" style="border-left: 5px solid #990000">
			<form class="form-horizontal" role="form" style="margin-top: 1%" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
				<div class="form-group">
			      <div class="col-sm-4" style="margin-right: 0px; padding-right: 5px;">
			        <input type="text" class="form-control" placeholder="First Name" name="firstname" required>
			      </div>

			      <div class="col-sm-4" style="margin-left: 0px;padding-left: 5px;">
			        <input type="text" class="form-control" placeholder="Last Name" name="lastname" required>
			      </div>
			    </div>
			    <div class="form-group">
			      <div class="col-sm-8">
			        <input type="date" class="form-control" placeholder="Date of Birth" name="dob" required>
			      </div>
			    </div>
			    <div class="form-group">
			      <div class="col-sm-8">
			        <input type="email" class="form-control" id="email" placeholder="E-mail" name="email" required>
			      </div>
			    </div>
			    <div class="form-group">
			      <div class="col-sm-8">
			        <input type="text" class="form-control" placeholder="Username" name="username" required>
			      </div>
			    </div>
			    <div class="form-group">
			      <div class="col-sm-8">          
			        <input type="password" class="form-control" placeholder="Enter password" name="password" required>
			      </div>
			    </div>
			    <div class="form-group">
			      <div class="col-sm-8">          
			        <input type="password" class="form-control" placeholder="Confirm password" name="confirmpassword" required>
			      </div>
			    </div>
			    <div class="form-group">        
			      <div class="col-sm-10">
			        <input type="submit" name="signup" value="Sign Up" class="btn btn-default">
			      </div>
			    </div>
			  </form>
		</div>
		<?php 
			if(isset($_POST['login'])) {
				$username = $_POST['username'];
				//$password = md5($_POST['password']);
				$password = md5($_POST['password']);
				$url = "";
				$sql = sprintf("SELECT * FROM user WHERE username='%s' AND password='%s' AND userType<>'new'",$username, $password);
				$check_login = mysqli_query($conn, $sql);

				if(mysqli_num_rows($check_login) == 1) {
					//session_start();
					$row = mysqli_fetch_array($check_login);
					//echo $row['userType'];
					if($row['userType'] == "member") {
						$_SESSION['member_login'] = true;
						$_SESSION['name'] = $username;
						$url = "pages/home.php";
					//	die('passed :)');
					}
					else {
						$_SESSION['admin_login'] = true;
						$_SESSION['name'] = $username;
						$url = "admin/admin.php";
					//	die('passed :)');	
					}
					header('Location: '.$url);
				}
				else {
					$sql = sprintf("SELECT * FROM user WHERE username='%s'",$username);
					$check_user = mysqli_query($conn, $sql);
					if(mysqli_num_rows($check_user) == 1) {
						echo "wrong password or user not approved yet";
					}
					else {
						echo "user does not exist";
					}
				}
			}

			if(isset($_POST['signup'])) {
				echo "working";
				$name = $_POST['firstname']." ".$_POST['lastname'];
				$username = $_POST['username'];
				$dob = $_POST['dob'];
				$password = md5($_POST['password']);
				$confirmpassword = md5($_POST['confirmpassword']);
				$email = $_POST['email'];

				if($password != $confirmpassword) {
					echo "try again";
				}
				else {
					$sql = sprintf("SELECT * FROM user WHERE name='%s'",$username);
					$check_user = mysqli_query($conn, $sql);
					if(mysqli_num_rows($check_user) == 1) {
						echo "username already exists";
					}
					else {
						$sql = sprintf("SELECT * FROM user WHERE email='%s'",$email);
						$check_user = mysqli_query($conn, $sql);
						if(mysqli_num_rows($check_user) == 1) {
							echo "email already exists";
						}
						else {
							$sql = sprintf("INSERT INTO user (name, username, dob, email, userType, password) VALUES ('%s','%s','%s','%s','new','%s');", $name,$username,$dob,$email, $password);
							if ($conn->query($sql) === TRUE) {
							    echo "Registration Successfull";
							} else {
							    echo "Error creating TABLE: " . $conn->error;
							}
						}
					}
				}
			}
		?>
	</div>
</body>
</html>