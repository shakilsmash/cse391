<?php 
include "dbconnect.php";


$sql = "CREATE TABLE anime (
	id INT AUTO_INCREMENT,
	name VARCHAR(50),
	rate INT,
	summary TEXT,
	date_of_release DATE,
	number_of_eps INT,
	status VARCHAR(10),
	PRIMARY KEY(id)
)";
if ($conn->query($sql) === TRUE) {
    echo "TABLE anime CREATED successfully\n";
} else {
    echo "Error creating TABLE: " . $conn->error;
}

// -------------------------------------------------------

$sql = "CREATE TABLE genre (
	id INT AUTO_INCREMENT,
	name VARCHAR(50),
	PRIMARY KEY(id)
)";


if ($conn->query($sql) === TRUE) {
    echo "TABLE genre CREATED successfully\n";
} else {
    echo "Error creating TABLE: " . $conn->error;
}

// -------------------------------------------------------


$sql = "CREATE TABLE anime_genre (
	genre_id INT,
	anime_id INT,
	FOREIGN KEY (genre_id) REFERENCES genre (id)
	ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (anime_id) REFERENCES anime (id)
	ON DELETE CASCADE ON UPDATE CASCADE
)";
if ($conn->query($sql) === TRUE) {
    echo "TABLE anime_genre CREATED successfully\n";
} else {
    echo "Error creating TABLE: " . $conn->error;
}


// -------------------------------------------------------

$sql = "CREATE TABLE user (
	id INT AUTO_INCREMENT,
	name VARCHAR(50),
	username VARCHAR(50),
	dob VARCHAR(50),
	email VARCHAR(50),
	userType VARCHAR(50),
	password VARCHAR(100),
	PRIMARY KEY(id)
)";
if ($conn->query($sql) === TRUE) {
    echo "TABLE user CREATED successfully\n";
} else {
    echo "Error creating TABLE: " . $conn->error;
}

// -------------------------------------------------------

$sql = "CREATE TABLE fav_list (
	anime_id INT,
	user_id INT,
	FOREIGN KEY (anime_id) REFERENCES anime (id)
	ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (user_id) REFERENCES user (id)
	ON DELETE CASCADE ON UPDATE CASCADE
)";
if ($conn->query($sql) === TRUE) {
    echo "TABLE fav_list CREATED successfully\n";
} else {
    echo "Error creating TABLE: " . $conn->error;
}

// -------------------------------------------------------

$sql = "CREATE TABLE fav_genre (
	genre_id INT,
	user_id INT,
	FOREIGN KEY (genre_id) REFERENCES genre (id)
	ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (user_id) REFERENCES user (id)
	ON DELETE CASCADE ON UPDATE CASCADE
)";
if ($conn->query($sql) === TRUE) {
    echo "TABLE fav_genre CREATED successfully\n";
} else {
    echo "Error creating TABLE: " . $conn->error;
}

// -------------------------------------------------------

$sql = "CREATE TABLE anime_comment (
	id INT AUTO_INCREMENT,
	approval_status VARCHAR(50),
	comment TEXT,
	anime_id INT,
	post_time DATE,
	PRIMARY KEY(id),
	FOREIGN KEY (anime_id) REFERENCES anime (id)
	ON DELETE CASCADE ON UPDATE CASCADE
)";
if ($conn->query($sql) === TRUE) {
    echo "TABLE anime_comment CREATED successfully\n";
} else {
    echo "Error creating TABLE: " . $conn->error;
}


// -------------------------------------------------------

$sql = "CREATE TABLE forum (
	id INT AUTO_INCREMENT,
	approval_status VARCHAR(50),
	title VARCHAR(100),
	user_id INT,
	PRIMARY KEY(id),
	FOREIGN KEY (user_id) REFERENCES user (id)
	ON DELETE CASCADE ON UPDATE CASCADE
)";
if ($conn->query($sql) === TRUE) {
    echo "TABLE forum CREATED successfully\n";
} else {
    echo "Error creating TABLE: " . $conn->error;
}

// -------------------------------------------------------

$sql = "CREATE TABLE forum_comment (
	id INT AUTO_INCREMENT,
	approval_status VARCHAR(50),
	comment TEXT,
	thread_id INT,
	post_time DATE,
	PRIMARY KEY(id),
	FOREIGN KEY (thread_id) REFERENCES forum (id)
	ON DELETE CASCADE ON UPDATE CASCADE
)";
if ($conn->query($sql) === TRUE) {
    echo "TABLE forum_comment CREATED successfully\n";
} else {
    echo "Error creating TABLE: " . $conn->error;
}


// -------------------------------------------------------

$sql = "CREATE TABLE chat_thread (
	id INT AUTO_INCREMENT,
	msg TEXT,
	user_id INT,
	post_time DATE,
	PRIMARY KEY(id),
	FOREIGN KEY (user_id) REFERENCES user (id)
	ON DELETE CASCADE ON UPDATE CASCADE
)";
if ($conn->query($sql) === TRUE) {
    echo "TABLE chat_thread CREATED successfully\n";
} else {
    echo "Error creating TABLE: " . $conn->error;
}

// -------------------------------------------------------

$sql = "CREATE TABLE msg (
	id INT AUTO_INCREMENT,
	msg TEXT,
	post_time DATE,
	user_id_to INT,
	user_id_from INT,
	PRIMARY KEY(id),
	FOREIGN KEY (user_id_to) REFERENCES user (id)
	ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (user_id_from) REFERENCES user (id)
	ON DELETE CASCADE ON UPDATE CASCADE
)";

if ($conn->query($sql) === TRUE) {
    echo "TABLE msg CREATED successfully\n";
} else {
    echo "Error creating TABLE: " . $conn->error;
}

// -------------------------------------------------------

$sql = "INSERT INTO genre (name) VALUES ('Action')";
mysqli_query($conn, $sql);
$sql = "INSERT INTO genre (name) VALUES ('Adventure')";
mysqli_query($conn, $sql);
$sql = "INSERT INTO genre (name) VALUES ('Comedy')";
mysqli_query($conn, $sql);
$sql = "INSERT INTO genre (name) VALUES ('Drama')";
mysqli_query($conn, $sql);
$sql = "INSERT INTO genre (name) VALUES ('Fantasy')";
mysqli_query($conn, $sql);
$sql = "INSERT INTO genre (name) VALUES ('Horror')";
mysqli_query($conn, $sql);
$sql = "INSERT INTO genre (name) VALUES ('Mecha')";
mysqli_query($conn, $sql);
$sql = "INSERT INTO genre (name) VALUES ('Mystery')";
mysqli_query($conn, $sql);
$sql = "INSERT INTO genre (name) VALUES ('Romance')";
mysqli_query($conn, $sql);
$sql = "INSERT INTO genre (name) VALUES ('Sci-Fi')";
mysqli_query($conn, $sql);
$sql = "INSERT INTO genre (name) VALUES ('Slice of Life')";
mysqli_query($conn, $sql);

?>