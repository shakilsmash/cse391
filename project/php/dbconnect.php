<?php
$servername = "localhost";
$username = "root";
$password = "";
//$dbName = "370project";
$dbName = "cse391_project";
// CREATE connection
$conn = mysqli_connect($servername, $username, $password, $dbName);

session_start();

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}